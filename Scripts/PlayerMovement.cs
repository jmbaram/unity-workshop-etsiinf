﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Camera cam;

    public float sensitivity = 10f;
    public float maxYAngle = 30f;
    public float maxXAngle = 60f;
    private Vector2 currentRotation;

    void Update()
    {
        currentRotation.x += Input.GetAxis("Mouse X") * sensitivity;
        currentRotation.y -= Input.GetAxis("Mouse Y") * sensitivity;
        currentRotation.x = Mathf.Clamp(currentRotation.x, -maxXAngle, maxXAngle);
        currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);
        cam.transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);
    }
}
