﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour {

    private UIController script;
    private bool ready;

    private void Start()
    {
        // Reseting ball.
        script = (UIController)GameObject.Find("Score").GetComponent(typeof(UIController));
        ready = false;
        StartCoroutine(SleepScoring());
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!ready) return;

        //Debug.Log("Collision with " + collision.gameObject.tag);
        if (collision.gameObject.tag == "Player")
        {
            script.UpdateScore(10);
        }
        else if (collision.gameObject.tag == "Brick")
        {
            script.UpdateScore(2);
        }
        else if (collision.gameObject.tag == "Level")
        {
            script.UpdateScore(5);
        }

        if (tag == "TNT")
        {
            GetComponent<Rigidbody>().AddExplosionForce(100.0f, transform.position, 10.0f, 5.0f, ForceMode.Impulse);
            ParticleSystem explosion = GetComponentInChildren<ParticleSystem>();
            explosion.Play();
            Destroy(gameObject, explosion.main.duration);
            script.UpdateScore(-10);
        }
    }

    IEnumerator SleepScoring()
    {
        yield return new WaitForSeconds(0.25f);
        ready = true;
    }
}
