﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    private Vector3 startPosition;
    private Quaternion startRotation;
    private Transform startParent;

    public bool playable;

    // Use this for initialization
    void Start () {

        // Record starting values of relevant variables
        startPosition = transform.localPosition;
        startRotation = transform.localRotation;
        startParent = transform.parent;
        playable = true;
    }

    // Called from controller.
    public void RemoteReset()
    {
        playable = false;
        ResetBall();

    }

    // Call this when the enemy needs to be killed and reset
    private void ResetBall()
    {
        GetComponent<Rigidbody>().isKinematic=true;
        transform.SetParent(startParent);
        transform.localPosition = startPosition;
        transform.localRotation = startRotation;
        playable = true;
    }

}
