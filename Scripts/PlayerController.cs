﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [Range(0.1f, 10.0f)]
    public float fillSpeed = 0.1f;

    [Range(10.0f, 100.0f)]
    public float ballForce = 50.0f;

    public Slider power;
    public Rigidbody ball;

    private BallController ballController;

    public void Start()
    {
        //power = GameObject.Find("Slider").GetComponent<Slider>();
        //ball = GameObject.Find("Ball").GetComponent<Rigidbody>();
        //Debug.Log(power.value);

        Cursor.visible = false;
        ballController = (BallController)ball.GetComponent(typeof(BallController));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (ballController.playable)
        {
            if (Input.GetButton("Fire1"))
            {
                power.value = Mathf.Clamp01(power.value + Time.deltaTime * fillSpeed / 10);
                //Debug.Log("Fire pressed: " + power.value);
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                ball.isKinematic = false;
                ball.transform.parent = null;
                ball.AddRelativeForce(new Vector3(0.0f, 100.0f, power.value * ballForce * 20.0f));

                ballController.playable = false;
 
                // Reset ball or next Level (Choose one).
                Invoke("ResetScene",5.0f);
                //Invoke("ResetBall",5.0f);

                power.value = 0.0f;
                Debug.Log("Waiting: " + power.value);

            }
        }
        
    }

    private void ResetScene()
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
    }

    private void ResetBall()
    {
        ballController.RemoteReset();
    }
}
