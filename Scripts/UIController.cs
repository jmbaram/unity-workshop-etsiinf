﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    private float score;

	// Use this for initialization
	void Start () {
        score = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateScore(float newScore)
    {

        score += newScore;
        GetComponent<Text>().text = "Score: " + score.ToString();

    }
}
